# Cell Segmentation GUI

This project contains the graphical interface application (segmentGUI.m), and a repository (src) with utilized functions as well as with additional plotting and visualizing scripts, and common parameters for certain kinds of cells and imaging conditions (parameters). Input is typically a folder containing images in TIFF or .mat files out of [MicroscopeControl](https://gitlab.com/MEKlab/MicroscopeControl).

The algorithm of cell segmentation is based on cell-edge detection by passing an input image through a low-pass filter. Further details can be found in [S. Jaramillo-Riveri's 2019 thesis](https://era.ed.ac.uk/handle/1842/36092) method section. Segmentation results can be manualy curated and exported into matlab files.
