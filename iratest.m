addpath(genpath('./src/')); % add paths

% filter to be used to calculate score
filterfile = '~/phd/MEK/Segment/src/filters/NA-9_DM-4.0_SG-1.00.mat';
% it adds variables: filts,angles,na
load(filterfile); 

%folder = '~/Desktop/16Oct/Rec+/';
folder   = '~/Desktop/16Oct/Rec+/';
filename = 'RecB-18_w25 mCherry.TIF';
filepath = [folder,filename];

params = struct;
params.minint = 500;   % minimum intensity
params.minscore = 1.5; % minimum score
params.minarea  = 2;   % minimum area

display(['----- Analysing file: ',filepath]);

% load stack
[stack,nstacks] = loadTIFstack(filepath);
% maximum project
img = maxproject(stack);
% compute score
[score] = getSPOTscore(img,filts,angles,na);
% theshold the image
mask0 = mytheimage(img,params.minint);
% theshold score
mask    = mytheimage(score,params.minscore);
% mask is a combination of both thesholds
mask2   = mask0.*mask;
% remove small components
mask2   = bwareaopen(mask,params.minarea);

% These figures could be useful to check parameters
% $$$ figure();
% $$$ subplot(2,2,1);
% $$$ imagesc(img);colormap('gray');
% $$$ subplot(2,2,2);
% $$$ imagesc(score);colormap('gray');
% $$$ subplot(2,2,3);
% $$$ imagesc(mask);colormap('gray');
% $$$ subplot(2,2,4);
% $$$ imagesc(mask2);colormap('gray');

% Make a cell with all spots and some info
statprops = {'PixelIdxList','Centroid','Area'};
B      = bwboundaries(mask);
cc     = bwconncomp(mask);
stats  = regionprops(cc,statprops{:});
spots  = cell(length(stats),1);
for i = 1:length(stats)
    center                = [stats(i).Centroid(1),stats(i).Centroid(2)];
    spots{i}              = struct;
    spots{i}.center       = center;
    spots{i}.border       = B{i};
    spots{i}.PixelIdxList = stats(i).PixelIdxList;
    spots{i}.area         = stats(i).Area;
    spots{i}.meanint      = mean(img(stats(i).PixelIdxList));
    spots{i}.maxint       = max(img(stats(i).PixelIdxList));
end

% Example of sumary results
figure();
% original image
subplot(2,2,1);
imagesc(img);colormap('gray');
% score
subplot(2,2,2);
imagesc(score);colormap('gray');
% original image + spots
subplot(2,2,3);
imagesc(img);colormap('gray');
hold on;
for n = 1:length(spots)
    plot(spots{n}.border(:,2),spots{n}.border(:,1),'r');
end
% max intensity of each spot
subplot(2,2,4);
simg = min(img(:))*ones(size(img));
for n = 1:length(spots)
    simg(spots{n}.PixelIdxList) = spots{n}.maxint;
end
imagesc(simg);colormap('gray');

%% Next is some other stuff... don't pay attention


% $$$ params = struct;
% $$$ params.gfmu    = 3;
% $$$ params.gfsig   = 1;
% $$$ params.scut    = -0.08;
% $$$ params.minarea = 50;
% $$$ params.maxarea = 10000;
% $$$ params.ustack = 5;
% $$$ 
% $$$ files  = dir(folder);
% $$$ fcount = 0;
% $$$ for j = 1:length(files)
% $$$     file = files(j).name;
% $$$     if(regmbool(file,'Brightfield')...
% $$$        && (~regmbool(file,'_cells')) ...
% $$$        && (regmbool(file,'S_')))
% $$$         
% $$$         filterfile = '~/phd/MEK/Segment/src/filters/NA-9_DM-5.0_SG-2.00.mat';
% $$$         load(filterfile); % filts,angles,na
% $$$ 
% $$$         fcount = fcount+1;
% $$$ % $$$         result{fcount} = struct;
% $$$ 
% $$$         filepath = [folder,file];
% $$$         display(['----- Analysing file: ',filepath]);
% $$$         
% $$$         [stack,nstacks] = loadTIFstack(filepath);
% $$$         
% $$$         img = stack(:,:,1); % pick last
% $$$ % $$$         img = stack(:,:,params.ustack); % pick last
% $$$         if(params.gfmu>0)
% $$$             img = imfilter(img,fspecial('gaussian',params.gfmu,params.gfsig),'replicate');
% $$$         end
% $$$         
% $$$         [nx,ny] = size(img);
% $$$         fimgs = zeros(nx,ny,na);
% $$$         for i = 1:na
% $$$             fimgs(:,:,i) = imfilter(img,filts{i},'replicate','conv');
% $$$         end
% $$$         fimgs2 = fimgs(:,:,1:na/2)+fimgs(:,:,na/2+1:end);
% $$$         
% $$$         score    = zeros(nx,ny);
% $$$         score(:) = sum(reshape(fimgs2.*real(acos(-1*pi*(fimgs2./abs(fimgs2)))./pi),nx*ny,na/2)');
% $$$         score    = score.*img;
% $$$         score    = -1*score./(max(max(score)));
% $$$         
% $$$         mask     = mytheimage(score,params.scut); % score threshold
% $$$         mask2    = mask-bwareaopen(mask,params.maxarea); 
% $$$         mask2    = double(imfill(bwareaopen(mask2,params.minarea),'holes'));
% $$$ 
% $$$         figure();
% $$$         subplot(2,2,1);
% $$$         imagesc(img);colormap('gray');
% $$$         subplot(2,2,2);
% $$$         imagesc(score);colormap('gray');
% $$$         subplot(2,2,3);
% $$$         imagesc(mask);colormap('gray');
% $$$         subplot(2,2,4);
% $$$         imagesc(mask2);colormap('gray');
% $$$         
% $$$ % $$$         result{fcount}.filepath = filepath;
% $$$ % $$$         result{fcount}.mask = mask2;
% $$$     elseif(regmbool(file,'mCherry'))
% $$$         filterfile = '~/phd/MEK/Segment/src/filters/NA-9_DM-4.0_SG-1.00.mat';
% $$$         load(filterfile); % filts,angles,na
% $$$         
% $$$         filepath = [folder,file];
% $$$         display(['----- Analysing file: ',filepath]);
% $$$         
% $$$         [stack,nstacks] = loadTIFstack(filepath);
% $$$ 
% $$$         img = maxproject(stack);
% $$$         
% $$$         mask0 = mytheimage(img,500);
% $$$         %img   = imfilter(img,fspecial('gaussian',5,0.75),'replicate');
% $$$         
% $$$         [score] = getSPOTscore(img,filts,angles,na);
% $$$         mask    = mytheimage(score,1.5);
% $$$         mask2   = mask0.*mask;
% $$$         mask2    = bwareaopen(mask,2);
% $$$         
% $$$         figure();
% $$$         subplot(2,2,1);
% $$$         imagesc(img);colormap('gray');
% $$$         subplot(2,2,2);
% $$$         imagesc(score);colormap('gray');
% $$$         subplot(2,2,3);
% $$$         imagesc(mask);colormap('gray');
% $$$         subplot(2,2,4);
% $$$         imagesc(mask2);colormap('gray');
% $$$         
% $$$     end
% $$$ end
