addpath(genpath('~/phd/MEK/Segment/src/'));

%% Convert segmentation to mean fluo and local backgrounds
segfiles = {'20170811_e206_Gly_0Cm_2_segmented.mat',...
            '20170811_e302_Gly_0Cm_segmented.mat',...
            '20170811_e206_Gly_2Cm_segmented.mat',...
            '20170811_e302_Gly_2Cm_segmented.mat'};
for i = 1:length(segfiles)
    segfile = segfiles{i};
    display(['Analyzing ',segfile]);
    temp = load(segfile);
    [areas,fluos,bkgs] = segment2fluo(temp.frameset,3,1.8,'~/phd_microscopy/image/Analysis_201707/');
    date = getdate();
    save([temp.prefix,'_quantification.mat'],'areas','fluos','bkgs','date');
end

%% Load files
quafiles = {'20170811_e206_Gly_0Cm_2_quantification.mat',...
            '20170811_e302_Gly_0Cm_quantification.mat',...
            '20170811_e206_Gly_2Cm_quantification.mat',...
            '20170811_e302_Gly_2Cm_quantification.mat'};
for i = 1:length(quafiles)
    temp = load(quafiles{i});
    careas{i} = temp.areas;
    cfluos{i} = temp.fluos;
    cbkgs{i}  = temp.bkgs;
end

titles = {'0 pal','2 pal','0 pal + Cm','2 pal + Cm'};
outfolder = 'CmPlots/';

%% Scatter plots
fig1 = figure('Name','Fluo raw');
for i = 1:4
    subplot(2,2,i); hold on;
    title(titles{i});
    myscatterplot(cfluos{i}(:,3),cfluos{i}(:,2),500,30000,200,60000,1);
    xlabel('mKate');ylabel('GFP');
end
fig1.PaperUnits = 'centimeters';
fig1.PaperPosition = [0 0 20 20];
print(fig1,[outfolder,'20170811_GlyCm_RawScatter.png'],'-dpng');
close(fig1);

fig2 = figure();
for i = 1:4
    subplot(2,2,i); hold on;
    title(titles{i});
    myscatterplot(careas{i},cfluos{i}(:,2),10,2000,200,60000,1);
    xlabel('area');ylabel('GFP');
end
fig2.PaperUnits = 'centimeters';
fig2.PaperPosition = [0 0 20 20];
print(fig2,[outfolder,'20170811_GlyCm_AreaScatter.png'],'-dpng');
close(fig2);

fig3 = figure('Name','Fluo normalized');
for i = 1:4
    subplot(2,2,i); hold on;
    title(titles{i});
    myscatterplot(cfluos{i}(:,3)-cbkgs{i}(:,3),cfluos{i}(:,2)-cbkgs{i}(:,2),10,20000,1,60000,1);
    xlabel('mKate');ylabel('GFP');
end
fig3.PaperUnits = 'centimeters';
fig3.PaperPosition = [0 0 20 20];
print(fig3,[outfolder,'20170811_GlyCm_NormScatter.png'],'-dpng');
close(fig3);

%% Histogram plots
fig4 = figure('Name','Histograms');
gfpbins = [logspace(-2,2.5,32)];
subplot(1,3,1); hold on;
for i = 1:4
    vals = (cfluos{i}(:,2)-cbkgs{i}(:,2))./mean((cfluos{1}(:,2)-cbkgs{1}(:,2)));
    h = relhist(vals,gfpbins);
    plot(gfpbins,h,'LineWidth',3);
end
axis([1e-1 10^2.5 0 0.3]);
set(gca,'xscale','log');
set(gca,'XTick',[0.1,1,10,100]);
set(gca,'YTick',[0,0.1,0.2,0.3]);
set(gca,'fontsize',20);
xlabel('GFP');

mchbins = [logspace(-1,1,32)];
subplot(1,3,2); hold on;
for i = 1:4
    vals = (cfluos{i}(:,3)-cbkgs{i}(:,3))./mean((cfluos{1}(:,3)-cbkgs{1}(:,3)));
    h = relhist(vals,mchbins);
    plot(mchbins,h,'LineWidth',3);
end
axis([1e-1 10 0 0.21]);
set(gca,'xscale','log');
set(gca,'XTick',[0.1,1,10,100]);
set(gca,'YTick',[0,0.1,0.2,0.3]);
set(gca,'fontsize',20);
xlabel('mKate');

areabins = [logspace(-1,1,32)];
subplot(1,3,3); hold on;
for i = 1:4
    vals = careas{i}./mean(careas{1});
    h = relhist(vals,areabins);
    plot(areabins,h,'LineWidth',3);
end
%axis([1e-1 10 0 0.21]);
set(gca,'xscale','log');
set(gca,'XTick',[0.1,1,10,100]);
set(gca,'YTick',[0,0.1,0.2,0.3]);
set(gca,'fontsize',20);
xlabel('area');
legend(titles);

fig4.PaperUnits = 'centimeters';
fig4.PaperPosition = [0 0 30 10];
print(fig4,[outfolder,'20170811_GlyCm_Histogram.png'],'-dpng');
close(fig4);
