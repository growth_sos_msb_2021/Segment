function [vals] = readborder(M,border)

[nq,~] = size(border);
vals = zeros(nq,1);
[nx,ny] = size(M);

for q = 1:nq
    p1 = max([round(border(q,1)),1]);
    p2 = max([round(border(q,2)),1]);
    if(p1<0)
        p1=1;
    end
    if(p2<0)
        p2=1;
    end
    if(p1>nx)
        p1=nx;
    end
    if(p2>ny)
        p2=ny;
    end
    vals(q) = M(p1,p2);
end

end