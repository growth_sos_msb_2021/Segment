function [img] = displaceimg(img,d,angle,sig)

tempgrid = zeros(7,7);

x = cos(pi*angle/180)*d;
y = sin(pi*angle/180)*d;
filt = normdiscmat(tempgrid,[x,y],sig);

img = imfilter(img,filt,'replicate');%,'conv'

end