function [box] = getbox(pixs,xmat,ymat,deltapix)
    [nx,ny] = size(xmat);
    box    = [0,0,0,0]; % x0,x1,y0,y1
    box(1) = max([1,min(xmat(pixs))-deltapix]);
    box(2) = min([nx,max(xmat(pixs))+deltapix]);
    %box(3) = max([1,min(ymat(pixs))]);
    %box(4) = min([ny,max(ymat(pixs))]);
    box(3) = max([1,min(ymat(pixs))-floor(deltapix/2)]);
    box(4) = min([ny,max(ymat(pixs)+floor(deltapix/2))]);
end
