function [tot] = sum2(mat)

[tot] = sum(sum(mat));
end