function [lenmat,aremat,widmat,cenmat1,cenmat2] = getlengthmatrix(linmat,frameset,xy,chn)

[~,nxy,~,nits] = size(frameset);

% Retrieve the cell length for each cell in each frame
lenmat  = NaN*zeros(size(linmat));
aremat  = NaN*zeros(size(linmat));
widmat  = NaN*zeros(size(linmat));
cenmat1 = NaN*zeros(size(linmat));
cenmat2 = NaN*zeros(size(linmat));
for c = 1:size(linmat,1)
    for it = 1:nits
        if(~isnan(linmat(c,it)))
            lenmat(c,it)  = frameset{1,xy,chn,it}.cells{linmat(c,it)}.length;
            aremat(c,it)  = frameset{1,xy,chn,it}.cells{linmat(c,it)}.area;
            widmat(c,it)  = frameset{1,xy,chn,it}.cells{linmat(c,it)}.width;
            cenmat1(c,it) = frameset{1,xy,chn,it}.cells{linmat(c,it)}.center(1);
            cenmat2(c,it) = frameset{1,xy,chn,it}.cells{linmat(c,it)}.center(2);
        end
    end
end


end