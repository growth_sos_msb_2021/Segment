function [frameset2] = interactivefix(frameset,chn,xy,params)

[~,nxy,~,nits] = size(frameset);

frameset2 = frameset;

%for xy = 1:nxy
[linmat,parmat,chlmat] = lineagematrix(frameset,chn,xy,params);

% Retrieve the cell length for each cell in each frame
lenmat = NaN*zeros(size(linmat));
for c = 1:size(linmat,1)
    for it = 1:nits
        if(~isnan(linmat(c,it)))
            lenmat(c,it) = frameset{1,xy,chn,it}.cells{linmat(c,it)}.length;
        end
    end
end

% Find events with abnormal increase in length, and ask user what to do
% about them
clrat = params.criticalratio;
tofix = [];
count = 0;
for c = 1:size(linmat,1)
    ls    = lenmat(c,:);
    divs  = chlmat(c,:);
    pos   = find(~isnan(ls)==1);
    lrat  = ls(2:end)./ls(1:end-1);
    % problematic points are
    probls = find(lrat>clrat);
    for ps = 1:length(probls)
        % ask what to do...
        p   = probls(ps);
        fig = figure('Position',[200,200,1000,800]);
        subplot(2,4,1:2); hold on;
        title(['Lineage ',num2str(c)]);
        plot(log(ls),'b.-');
        plot(log(mean(ls(pos)))*divs./divs,'go');
        plot(p+1,log(ls(p+1)),'ro');
        %plot(p,log(ls(p)),'ro');
        xlabel('Frame');
        ylabel('Log length');
        strs = {'None','Merge','Split','Rmv'};
        for k = 1:4
            popup(k) = uicontrol(fig,'Style','pushbutton','String',strs{k},...
                                 'FontSize',20,...
                                 'Position',[600+(k-1)*110,600,100,50],...
                                 'UserData',0,...
                                 'Callback',@pushaux);
        end
        strs = {'1','-1'};
        for k = 1:2
            edit(k) = uicontrol(fig,'Style','edit','String',strs{k},...
                                'FontSize',20,...
                                'Position',[600+(k)*110,550,100,50]);
        end
        strs = {'1','1'};
        for k = 1:2
            edit(2+k) = uicontrol(fig,'Style','edit','String',strs{k},...
                                  'FontSize',20,...
                                  'Position',[600+(k)*110,500,100,50]);
        end
        for itp = 1:4
            subplot(2,4,itp+4);
            pp = min([nits,max([1,p+itp-2])]);
            temp = load(frameset{1,xy,chn,pp}.filepath);
            img  = double(temp.img);
            img  = rotateimg(img,params.rot);
            imshow(img,[]); hold on;
            for nc = 1:length(frameset{1,xy,chn,pp}.cells)
                plot(frameset{1,xy,chn,pp}.cells{nc}.border(:,2),...
                     frameset{1,xy,chn,pp}.cells{nc}.border(:,1),'g');
            end
            if(~isnan(linmat(c,pp)))
                plot(frameset{1,xy,chn,pp}.cells{linmat(c,pp)}.border(:,2),...
                     frameset{1,xy,chn,pp}.cells{linmat(c,pp)}.border(:,1),'r');
            end
            title(['Frame ',num2str(pp)]);
        end
        set([fig,popup,edit],'Units','normalized');
        movegui(fig,'center');
        while((get(popup(1),'UserData')==0)&&...
              (get(popup(2),'UserData')==0)&&...
              (get(popup(3),'UserData')==0)&&...
              (get(popup(4),'UserData')==0))
            pause(1);
        end
        for k = 1:4
            if(get(popup(k),'UserData')==1)
                action = k-1;
                break;
            end
        end
        if(action>0)
            count = count+1;
            if(action==1) % Merge previous
                frame = p;
                withcell = str2num(get(edit(1),'String'));
                scope    = str2num(get(edit(3),'String'));
            elseif(action==2) % Split current
                frame = p+1;
                withcell = str2num(get(edit(2),'String'));
                scope    = str2num(get(edit(4),'String'));
            elseif(action==3) % Remove current
                frame    = p+1;
                scope    = 1;
                withcell = 0;
            end
            tofix = [tofix;...
                     c,frame,action,withcell,scope];
        end
        close(fig);
    end
end

helpfigs = 0;
% Do the fixes!
frameset2 = frameset;
replaceset = [];
count = 0;
for n = 1:size(tofix)
    c      = tofix(n,1);
    frame  = tofix(n,2);
    action = tofix(n,3);
    delta  = tofix(n,4);
    scope  = tofix(n,5);    
    if(action==1) % merge
        for it = (frame-scope+1):frame;     
            cells  = frameset{1,xy,chn,it}.cells;
            if(delta>0)
                c1 = linmat(c,it);
                c2 = linmat(c,it)+delta;
            else
                c1 = linmat(c,it)+delta;
                c2 = linmat(c,it);
            end
            mcell = mmmergecells(cells{c1},cells{c2});
            %cells = {cells{1:linmat(c,it)-1},mcell,cells{linmat(c,it)+delta+1:end}};
            %frameset2{1,xy,chn,it}.cells = cells;
            count = count+1;
            replaceset{count} = {it,[c1,c2],mcell};
        end
    elseif(action==2) % split
        if(delta<0)
            c1 = linmat(c,frame-1)+delta;
            c2 = linmat(c,frame-1);
        else
            c1 = linmat(c,frame-1);
            c2 = linmat(c,frame-1)+delta;
        end
        if((c1<1)||(c2<1)||isnan(c2))
            tofix(n,:)
            error('Error while splitting cells');
        end
        cell1 = frameset{1,xy,chn,frame-1}.cells{c1};
        cell2 = frameset{1,xy,chn,frame-1}.cells{c2};
        for it = frame:(frame+scope-1)
            cells  = frameset{1,xy,chn,it}.cells;
            mcell  = cells{linmat(c,it)};
            [scell1,scell2] = mmsplitcells(mcell,cell1,cell2);
            %cells = {cells{1:linmat(c,it)-1},scell1,scell2,cells{linmat(c,it)+1:end}};
            %frameset2{1,xy,chn,it}.cells = cells;
            count = count+1;
            replaceset{count} = {it,linmat(c,it),{scell1,scell2}};
        end
    elseif(action==3) % remove
        count = count+1;
        replaceset{count} = {frame,linmat(c,frame),{}};
    else
        error('WTH!');
    end
end

replaceframes = cell(nits,1);
for n = 1:length(replaceset)
    s = replaceset{n}; it = s{1};
    if(isempty(s{2}))
        error(['Error while correcting frame ',num2str(n)]);
    end
    replaceframes{it} = [replaceframes{it},n];
end

for it = 1:nits
    if(~isempty(replaceframes{it}))
        sets  = replaceframes{it};
        cells = frameset{1,xy,chn,it}.cells;
        % it is convinient to keep the order, so we will do this cumbersome way
        % of replacing
        rmvs  = zeros(length(cells),1);
        for n = 1:length(sets)
            s          = replaceset{sets(n)};
            cellCs     = s{2};
            cellStrucs = s{3};
            for k = 1:length(cellCs)
                rmvs(cellCs(k))=n;
            end
        end
        cells2 = {};
        for k = 1:length(cells)
            if(rmvs(k)==0)
                cells2 = {cells2{1:end},cells{k}};
            else
                s          = replaceset{sets(rmvs(k))};
                cellCs     = s{2};
                cellStrucs = s{3};
                if(isempty(cellStrucs))
                    % to be removed
                elseif((length(cellCs)>1) && cellCs(2)==k)
                    % Second of a merge, do nothing
                elseif(length(cellCs)>1)
                    % First of a merge, replace by 1
                    cells2 = {cells2{1:end},cellStrucs};
                else % is a split
                    cells2 = {cells2{1:end},cellStrucs{1},cellStrucs{2}};
                end
            end            
        end
        frameset2{1,xy,chn,it}.cells = cells2;
    end
end

%end

function [] = pushaux(Hobj,~)
    set(Hobj,'UserData',1);
    set(Hobj,'BackgroundColor','yellow');
end

end