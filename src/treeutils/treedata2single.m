function [singles,idRevMap,parents,childs] = treedata2single(data)

count     = 0;
singles   = [];
idRevMap  = [];
parents   = [];
for c = 1:size(data.lenmats,1)
    pos  = find(isnan(data.lenmats(c,:))==0);
    divs = find(isnan(data.chlmats(c,:))==0);
    if(~isempty(pos)) % has data
        if(~isempty(divs)) % it divides
            p0 = pos(1);
            for n = 1:length(divs) % create new rows for each division
                count = count+1;
                singles(count,:) = [c,p0,divs(n)-1];
                idRevMap(count) = c;
                p0 = divs(n);
                if(n==1)
                    parents(count)   = 0;
                else
                    parents(count)   = count-1;
                end
            end
        else % assing a single ID
            count = count+1;
            singles(count,:) = [c,pos(1),pos(end)];
            idRevMap(count)  = c;
            parents(count)   = 0;
        end
    end
end

tofix = find(parents==0);
for s = tofix
    c = idRevMap(s);
    if(isnan(data.parmats(c)))
        parents(s) = NaN;
    else
        p = data.parmats(c);
        scand = find(idRevMap==p);
        if(isempty(scand))
            error('WTH!');
        else
            init  = singles(s,2)-1;
            found = 0;
            for sc = 1:length(scand)
                % find the one!
                if(singles(scand(sc),3)==init)
                    parents(s) = scand(sc);
                    found = 1;
                    break;
                end                
            end
            if(found==0)
% $$$                 s
% $$$                 singles(s,:)
% $$$                 for sc = 1:length(scand)
% $$$                     singles(scand(sc),:)
% $$$                 end                
                error('Could not find parent');
            end
        end
    end
end

parents = parents';

childs = NaN*zeros(size(parents,1),2);
for s = 1:size(parents,1)
    if(~isnan(parents(s)))
        p = parents(s);
        if(isnan(childs(p,1)))
            childs(p,1) = s;
        elseif(isnan(childs(p,2)))
            childs(p,2) = s;
        else
            error('Parent with 2 childs!');
        end
    end
end

end