function [pos1,pos2,dmat,imat] = celldistancematrix(cells1,cells2)

%% Matrix is m,n where m is the dim of cell1 and n is the dim of cell2

n1 = length(cells1);
n2 = length(cells2);

pos1 = zeros(n1,2);
for i = 1:n1
    pos1(i,:) = cells1{i}.center;
end
pos2 = zeros(n2,2);
for i = 1:n2
    pos2(i,:) = cells2{i}.center;
end

dxs = pos1(:,1)'-pos2(:,1);
dys = pos1(:,2)'-pos2(:,2);

dmat = sqrt(dxs.^2+dys.^2);

[~,imat] = sort(dmat,2,'ascend');

end