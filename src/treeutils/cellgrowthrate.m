function [grates,r2s,sses,n4fits] = cellgrowthrate(cell,sint,wint)

lengths = smooth(cell.lengths,sint);
%lengths = cell.lengths';
    lvals   = log(lengths);
    times   = cell.times;
    
    grates = zeros(size(lvals));
    r2s    = zeros(size(lvals));
    sses   = zeros(size(lvals));
    n4fits = zeros(size(lvals));
    nvals  = length(lvals);
    
    for i = 1:nvals
        if(i-wint>=1 && i+wint<=nvals)
            vals  = lvals(i-wint:i+wint);
            tvals = times(i-wint:i+wint);
        elseif(i-wint<1) % from the left
            endp  = min([nvals,i+wint]);
            vals  = lvals(1:endp);
            tvals = times(1:endp);
        else
            stap  = max([1,nvals-(i+wint)]);
            vals  = lvals(stap:end);
            tvals = times(stap:end);
        end
        n4fits(i) = length(vals);
        [f0,stats] = fit(tvals',vals,'poly1');
        grates(i) = f0.p1;
        r2s(i)    = stats.rsquare;
        sses(i)   = stats.sse;
    end
end