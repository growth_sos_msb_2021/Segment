function [scell1,scell2] = mmsplitcells(mcell,cell1,cell2)

% Taking mcell, and split by cell1 and cell2
% The split will preserve the ratio of lengths from cell1 and cell2
% cell1 is assumed to be on top.

% $$$ f1 = cell1.length/(cell1.length+cell2.length);
% $$$ f2 = cell2.length/(cell1.length+cell2.length);
% $$$ 
% $$$ 
% $$$ [~,clen] = midline2length(mcell.midline);
% $$$ 
% $$$ clen = [clen;mcell.length];
% $$$ 
% $$$ % separate based on length
% $$$ pos1 = find(clen>f1*(mcell.length));
% $$$ pos1 = 1:pos1(1);
% $$$ pos2 = find((mcell.length-clen)>f1*(mcell.length));
% $$$ pos2 = pos2(end):length(clen);
% $$$ 
% $$$ % Populate cell struct
% $$$ scell1 = struct;
% $$$ scell2 = struct;
% $$$ 
% $$$ scell1.center = [0,0];
% $$$ scell2.center = [0,0];
% $$$ 
% $$$ % with will be all the same
% $$$ scell1.width = mcell.width;
% $$$ scell2.width = mcell.width;
% $$$ 
% $$$ % take midline
% $$$ scell1.midline = mcell.midline(pos1,:);
% $$$ scell2.midline = mcell.midline(pos2,:);
% $$$ 
% $$$ scell1.center = mean(scell1.midline);
% $$$ scell2.center = mean(scell2.midline);
% $$$ 
% $$$ % take edges
% $$$ scell1.edges = mcell.edges(pos1,:);
% $$$ scell2.edges = mcell.edges(pos2,:);
% $$$ 
% $$$ % fix the tips
% $$$ scell1.edges(end,1) = scell1.midline(end,1);
% $$$ scell1.edges(end,2) = scell1.midline(end,1);
% $$$ scell1.edges(end-1,1) = scell1.midline(end-1,1)-0.4*scell1.width;
% $$$ scell1.edges(end-1,2) = scell1.midline(end-1,1)+0.4*scell1.width;
% $$$ 
% $$$ scell2.edges(1,1) = scell2.midline(1,1);
% $$$ scell2.edges(1,2) = scell2.midline(1,1);
% $$$ scell2.edges(2,1) = scell2.midline(2,1)-0.4*scell2.width;
% $$$ scell2.edges(2,2) = scell2.midline(2,1)+0.4*scell2.width;
% $$$ 
% $$$ % transform to borders
% $$$ scell1.border  = [[scell1.midline(:,2),scell1.edges(:,1)];...
% $$$                   [flipud(scell1.midline(:,2)),flipud(scell1.edges(:,2))]];
% $$$ scell2.border  = [[scell2.midline(:,2),scell2.edges(:,1)];...
% $$$                   [flipud(scell2.midline(:,2)),flipud(scell2.edges(:,2))]];
% $$$ 
% $$$ % compute length and area
% $$$ scell1.length = midline2length(scell1.midline);
% $$$ scell2.length = midline2length(scell2.midline);
% $$$ scell1.area   = border2area(scell1);
% $$$ scell2.area   = border2area(scell2);

% For now this is good enough...
scell1 = cell1;
scell2 = cell2;

% TODO: PixelIdxList


end