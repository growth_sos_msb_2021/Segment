function [pixels] = border2pixels(img,cell)

mask = zeros(size(img));

xpos  = cell.midline(:,2);
edges = cell.edges;

for nx = 1:length(xpos)
    x  = round(xpos(nx));
    y1 = ceil(edges(nx,1));
    y2 = floor(edges(nx,2));
    if(~(y1==y2))
        mask(x,y1:y2) = 1;
    end
end

pixels = find(mask==1);

end 