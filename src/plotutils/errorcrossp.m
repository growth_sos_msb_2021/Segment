function [] = errorcrossp(x,y,xe,ye,col,lw,f,miny)

xm = max([miny,x-xe]);
ym = max([miny,y-ye]);
plot([xm,x+xe],[y,y],'color',col,'LineWidth',lw);
plot([x,x],[ym,y+ye],'color',col,'LineWidth',lw);

plot([xm,xm],[y+f(2),y-f(2)],'color',col,'LineWidth',lw);
plot([x+xe,x+xe],[(1)*y+f(2),(1)*y-f(2)],'color',col,'LineWidth',lw);
plot([(1)*x+f(1),(1)*x-f(1)],[ym,ym],'color',col,'LineWidth',lw);
plot([(1)*x+f(1),(1)*x-f(1)],[y+ye,y+ye],'color',col,'LineWidth',lw);

end