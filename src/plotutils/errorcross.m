function [] = errorcross(x,y,xe,ye,col,lw,f)

plot([x-xe,x+xe],[y,y],'color',col,'LineWidth',lw);
plot([x,x],[y-ye,y+ye],'color',col,'LineWidth',lw);

plot([x-xe,x-xe],[y+f(2),y-f(2)],'color',col,'LineWidth',lw);
plot([x+xe,x+xe],[(1)*y+f(2),(1)*y-f(2)],'color',col,'LineWidth',lw);
plot([(1)*x+f(1),(1)*x-f(1)],[y-ye,y-ye],'color',col,'LineWidth',lw);
plot([(1)*x+f(1),(1)*x-f(1)],[y+ye,y+ye],'color',col,'LineWidth',lw);

% $$$ plot([x-xe,x-xe],[(1+f(2))*y,(1-f(2))*y],'color',col,'LineWidth',lw);
% $$$ plot([x+xe,x+xe],[(1+f(2))*y,(1-f(2))*y],'color',col,'LineWidth',lw);
% $$$ plot([(1+f(1))*x,(1-f(1))*x],[y-ye,y-ye],'color',col,'LineWidth',lw);
% $$$ plot([(1+f(1))*x,(1-f(1))*x],[y+ye,y+ye],'color',col,'LineWidth',lw);

end